package ua.dnigma.fragment2fragment_2;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;



public class MainActivity extends AppCompatActivity implements FragmentPost {

    FrameLayout container;
    FragmentManager fragmentManager;
    Fragment_1 fragment_1;
    Fragment_2 fragment_2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        container = (FrameLayout) findViewById(R.id.container_frag);

        fragmentManager = getSupportFragmentManager();
        fragment_1 = new Fragment_1();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container_frag, fragment_1);
        fragmentTransaction.commit();



    }

    @Override
    public void fragmentPostOut(String str) {

        fragment_2.setTextView_fr2(str);

    }


}
