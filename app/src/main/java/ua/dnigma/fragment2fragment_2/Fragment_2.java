package ua.dnigma.fragment2fragment_2;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Даниил on 22.10.2016.
 */

public class Fragment_2 extends Fragment {

    public TextView textView_fr2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view2 = inflater.inflate(R.layout.fragment_2, container, false);
        return view2;
    }

    public void setTextView_fr2(String item_fr2) {
        textView_fr2 = (TextView) getView().findViewById(R.id.textView_f2);
        textView_fr2.setText(item_fr2);
    }
}
