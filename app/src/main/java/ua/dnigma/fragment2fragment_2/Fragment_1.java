package ua.dnigma.fragment2fragment_2;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import static ua.dnigma.fragment2fragment_2.R.layout.fragment_2;


public class Fragment_1 extends Fragment {

    private EditText editText_f1;
    private Button button_f1;
    private FragmentPost fragmentPost;
    FragmentManager fragmentManager;
    Fragment_2 fragment_2;

    @Override
    public void onAttach(Context context) {
        Activity activity = (Activity)context;
        super.onAttach(context);
        fragmentPost = (FragmentPost)activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view1 = inflater.inflate(R.layout.fragment_1, container, false);

        editText_f1 = (EditText) view1.findViewById(R.id.editText_f1);
        button_f1 = (Button) view1.findViewById(R.id.button_f1);

        button_f1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentPost.fragmentPostOut(editText_f1.toString());
                fragmentManager = getFragmentManager();
                fragment_2 = new Fragment_2();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_frag, fragment_2);
                fragmentTransaction.commit();
            }
        });
        return view1;
    }
}
